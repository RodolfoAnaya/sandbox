import { Component } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.scss']
})
export class ContadorComponent {

  value:number=0;

  change(opt:string='') {
    if(opt){
      this.value++;
    }else {
      this.value--;
    }
  }

  reset() {
    this.value=0;
  }

}
