import { Component } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.scss']
})
export class EventBindingComponent {

  message : string = '';

  onClick() {
    this.message='Hiciste click, ¡felicidades!'
  }

  onDelete() {
    this.message='';
  }

}
