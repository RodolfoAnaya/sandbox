import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MatListModule} from '@angular/material/list';

import { DataBindingRoutingModule } from './data-binding-routing.module';
import { EventBindingComponent } from './components/event-binding/event-binding.component';
import { PropertyBindingComponent } from './components/property-binding/property-binding.component';
import { TwoWayDataBindingComponent } from './components/two-way-data-binding/two-way-data-binding.component';
import { DataBindingComponent } from './view/data-binding/data-binding.component';


@NgModule({
  declarations: [
    TwoWayDataBindingComponent,
    PropertyBindingComponent,
    EventBindingComponent,
    DataBindingComponent
  ],
  imports: [
    CommonModule,
    DataBindingRoutingModule,
    FormsModule,
    MatListModule
  ]
})
export class DataBindingModule { }
