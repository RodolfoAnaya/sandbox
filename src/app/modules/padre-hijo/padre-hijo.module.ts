import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PadreHijoRoutingModule } from './padre-hijo-routing.module';
import { PadreHijoComponent } from './components/padre-hijo/padre-hijo.component';
import { PadreComponent } from './components/padre/padre.component';
import { HijoComponent } from './components/hijo/hijo.component';


@NgModule({
  declarations: [
    PadreHijoComponent,
    PadreComponent,
    HijoComponent
  ],
  imports: [
    CommonModule,
    PadreHijoRoutingModule
  ]
})
export class PadreHijoModule { }
