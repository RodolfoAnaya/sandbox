import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PadreHijoComponent } from './components/padre-hijo/padre-hijo.component';

const routes: Routes = [
  {
    path: '',
    component: PadreHijoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PadreHijoRoutingModule { }
