import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.scss']
})
export class HijoComponent implements OnInit {
  @Input() date:Date|undefined;
  @Output() name : EventEmitter<string> = new EventEmitter<string>();

  ngOnInit(): void {
    this.sendName('Tania')
  }

  sendName(name:string): void{
    this.name.emit(name);
  }

}
