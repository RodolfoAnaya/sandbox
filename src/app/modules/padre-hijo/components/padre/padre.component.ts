import { Component } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.scss']
})
export class PadreComponent {

  date=new Date();
  name:string='';

  onReceiveName(name:string) {
    this.name=name;
  }


}
