import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContadorModule } from '../contador/contador.module';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path:'padre-hijo',
        loadChildren: () => import('../padre-hijo/padre-hijo.module')
        .then(({PadreHijoModule}) => PadreHijoModule)
      },
      {
        path:'data-binding',
        loadChildren: () => import('../data-binding/data-binding.module')
        .then(({DataBindingModule}) => DataBindingModule)
      },
      {
        path:'contador',
        loadChildren: () => import('../contador/contador.module')
        .then(({ContadorModule}) => ContadorModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
